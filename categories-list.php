
<?php require_once ('./includes/header.php') ?>

<section class="page-header" style="background-image: url(assets/images/backgrounds/main-banner.jpg);">
            <div class="container">
                <h2>More Categories</h2>
                <!--
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="index.html">Home</a></li>
                    <li><span>Destination</span></li>
                </ul>--><!-- /.thm-breadcrumb -->
            </div><!-- /.container -->
        </section>

        <section class="tour-two tour-list">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="tour-sorter-one">
                            <h3>24 Tours Found</h3>
                            <div class="tour-sorter-one__right">
                                <div class="tour-sorter-one__select">
                                    <select name="sort-by" id="sort-by" class="selectpicker">
                                        <option value="Sort">Sort</option>
                                        <option value="By Date">By Date</option>
                                        <option value="By Price">By Price</option>
                                    </select><!-- /#sort-by .selectpicker -->
                                </div><!-- /.tour-sorter-one__select -->
                                <a  class="active" href="categories-list.php"><i class="tripo-icon-list-menu"></i></a>
                        <a  href="categories-thumb.php"><i class="tripo-icon-squares"></i></a>
                            </div><!-- /.tour-sorter-one__right -->

                        </div><!-- /.tour-sorter-one -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
                        <div class="tour-two__single tour-one__single tourlist">
                            <div class="tour-two__image-wrap">
                                <div class="tour-one__image">
                                    <img src="assets/images/tour/tour-2-1.jpg" alt="">
                                    <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                                </div><!-- /.tour-one__image -->
                            </div><!-- /.tour-two__image-wrap -->
                            <div class="tour-one__content ">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->

                        </div><!-- /.tour-two__single -->
               
                        <div class="post-pagination">
                            <a href="#"><i class="fa fa-angle-left"></i></a>
                            <a class="active" href="#">01</a>
                            <a href="#">02</a>
                            <a href="#">03</a>
                            <a href="#"><i class="fa fa-angle-right"></i></a>
                        </div><!-- /.post-pagination -->
                    </div><!-- /.col-lg-8 -->
                    <div class="col-lg-4">
                        <div class="tour-sidebar">
                            <div class="tour-sidebar__search tour-sidebar__single">
                                <h3>Search Tours</h3>
                                <form action="#" class="tour-sidebar__search-form">
                                    <div class="input-group">
                                        <input type="text" placeholder="Where to">
                                    </div><!-- /.input-group -->
                                    <div class="input-group">
                                        <input type="text" data-provide="datepicker" placeholder="When">
                                    </div><!-- /.input-group -->
                                    <div class="input-group">
                                        <select class="selectpicker">
                                            <option value="Type">Type</option>
                                            <option value="Adventure">Adventure</option>
                                            <option value="Wildlife">Wildlife</option>
                                            <option value="Sightseeing">Sightseeing</option>
                                        </select>
                                    </div><!-- /.input-group -->
                                    <div class="input-group">
                                        <button type="submit" class="thm-btn">Search</button>
                                    </div><!-- /.input-group -->
                                </form>
                            </div><!-- /.tour-sidebar__search -->
                            <div class="tour-sidebar__sorter-wrap">
                                <div class="tour-sidebar__sorter-single">
                                    <div class="tour-sidebar__sorter-top">
                                        <h3>Price</h3>
                                        <button class="tour-sidebar__sorter-toggler"><i class="fa fa-angle-down"></i></button>
                                        <!-- /.tour-sidebar__sorter-toggler -->
                                    </div><!-- /.tour-sidebar__sorter-top -->
                                    <div class="tour-sidebar__sorter-content">
                                        <div class="tour-sidebar__price-range">
                                            <div class="form-group">
                                                <p>$<span id="min-value-rangeslider"></span></p>
                                                <p>$<span id="max-value-rangeslider"></span></p>
                                            </div>
                                            <div class="range-slider-price" id="range-slider-price"></div>
                                        </div><!-- /.tour-sidebar__price-range -->

                                    </div><!-- /.tour-sidebar__sorter-content -->

                                </div><!-- /.tour-sidebar__sorter-single -->
                                <div class="tour-sidebar__sorter-single">
                                    <div class="tour-sidebar__sorter-top">
                                        <h3>Review Score</h3>
                                        <button class="tour-sidebar__sorter-toggler"><i class="fa fa-angle-down"></i></button>
                                        <!-- /.tour-sidebar__sorter-toggler -->
                                    </div><!-- /.tour-sidebar__sorter-top -->
                                    <div class="tour-sidebar__sorter-content">
                                        <div class="tour-sidebar__sorter-inputs">
                                            <p>
                                                <input type="checkbox" id="review-5" name="radio-group">
                                                <label for="review-5">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="review-4" name="radio-group">
                                                <label for="review-4">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star"></i>
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="review-3" name="radio-group">
                                                <label for="review-3">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star"></i>
                                                    <i class="fa fa-star"></i>
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="review-2" name="radio-group">
                                                <label for="review-2">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i>
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="review-1" name="radio-group">
                                                <label for="review-1">
                                                    <i class="fa fa-star active"></i>
                                                    <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i>
                                                    <i class="fa fa-star "></i>
                                                </label>
                                            </p>
                                        </div><!-- /.tour-sidebar__sorter-inputs -->
                                    </div><!-- /.tour-sidebar__sorter-content -->

                                </div><!-- /.tour-sidebar__sorter-single -->
                                <div class="tour-sidebar__sorter-single">
                                    <div class="tour-sidebar__sorter-top">
                                        <h3>Categories</h3>
                                        <button class="tour-sidebar__sorter-toggler"><i class="fa fa-angle-down"></i></button>
                                        <!-- /.tour-sidebar__sorter-toggler -->
                                    </div><!-- /.tour-sidebar__sorter-top -->
                                    <div class="tour-sidebar__sorter-content">

                                        <div class="tour-sidebar__sorter-inputs">
                                            <p>
                                                <input type="checkbox" id="cat-5" name="radio-group">
                                                <label for="cat-5">
                                                    City Tours
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="cat-4" name="radio-group">
                                                <label for="cat-4">HostedTours
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="cat-3" name="radio-group">
                                                <label for="cat-3">Adventure Tours
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="cat-2" name="radio-group">
                                                <label for="cat-2">Group Tours

                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="cat-1" name="radio-group">
                                                <label for="cat-1">
                                                    Couple Tours
                                                </label>
                                            </p>
                                        </div><!-- /.tour-sidebar__sorter-inputs -->
                                    </div><!-- /.tour-sidebar__sorter-content -->

                                </div><!-- /.tour-sidebar__sorter-single -->
                                <div class="tour-sidebar__sorter-single">
                                    <div class="tour-sidebar__sorter-top">
                                        <h3>Duration</h3>
                                        <button class="tour-sidebar__sorter-toggler"><i class="fa fa-angle-down"></i></button>
                                        <!-- /.tour-sidebar__sorter-toggler -->
                                    </div><!-- /.tour-sidebar__sorter-top -->
                                    <div class="tour-sidebar__sorter-content">

                                        <div class="tour-sidebar__sorter-inputs">
                                            <p>
                                                <input type="checkbox" id="duration-5" name="radio-group">
                                                <label for="duration-5">
                                                    0 - 24 hours
                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="duration-4" name="radio-group">
                                                <label for="duration-4">1 - 2 days

                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="duration-3" name="radio-group">
                                                <label for="duration-3">2 - 3 days

                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="duration-2" name="radio-group">
                                                <label for="duration-2">4 - 5 days


                                                </label>
                                            </p>
                                            <p>
                                                <input type="checkbox" id="duration-1" name="radio-group">
                                                <label for="duration-1">
                                                    5 - 10 days
                                                </label>
                                            </p>
                                        </div><!-- /.tour-sidebar__sorter-inputs -->
                                    </div><!-- /.tour-sidebar__sorter-content -->

                                </div><!-- /.tour-sidebar__sorter-single -->

                            </div><!-- /.tour-sidebar__sorter-wrap -->
                        </div><!-- /.tour-sidebar -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.tour-two -->



 <?php require_once ('./includes/footer.php') ?>