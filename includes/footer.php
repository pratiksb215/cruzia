<section class="newsletter-one">
            <div class="container">
                <div class="row">
                <div class="col-md-12 col-lg-6">
                    <abbr>Get Latest Tour Updates </abbr>
                </div>
                <div class="col-md-12 col-lg-6">
                    <ul>
                        <li><input type="text" placeholder="Email Address" /></li>
                        <li><button>Subscribe Now </button></li>
                    </ul>
                </div>
                </div>

            </div>
        </section>   
        
        
        
        <footer class="site-footer">
            <div class="site-footer__bg" style="background-image: url(assets/images/backgrounds/main-footer.jpg);"></div>
            <!-- /.site-footer__bg -->
            <div class="container">
                <div class="row">
                    <div class="footer-widget__column footer-widget__about">
                        <a href="index.php" class="footer-widget__logo"><img src="assets/images/logo-dark.png" width="80" alt=""></a>
                        <p>Tripo is a premium HTML Template for tours, travels, trips, adventures and a wide range of other
                            tour agencies.</p>
                        <a class="footer-link"  href="mailto:needhelp@tripo.com">needhelp@tripo.com</a> <br>
                        <a class="footer-link"  href="tel:888-999-0000">888 999 0000</a>
                    </div><!-- /.footer-widget__column -->
                    <div class="footer-widget__column footer-widget__links">
                        <h3 class="footer-widget__title">Company</h3><!-- /.footer-widget__title -->
                        <ul class="footer-widget__links-list list-unstyled">
                            <li><a href="about.html">About Us</a></li>
                            <li><a href="news.html">Community Blog</a></li>
                            <li><a href="about.html">Rewards</a></li>
                            <li><a href="contact.html">Work with Us</a></li>
                            <li><a href="tour-guides.html">Meet the Team</a></li>
                        </ul><!-- /.footer-widget__links-list list-unstyled -->
                    </div><!-- /.footer-widget__column -->
                    <div class="footer-widget__column footer-widget__links">
                        <h3 class="footer-widget__title">Links</h3><!-- /.footer-widget__title -->
                        <ul class="footer-widget__links-list list-unstyled">
                            <li><a href="destinations.html">Destinations</a></li>
                            <li><a href="tour-sidebar.html">Tours</a></li>
                            <li><a href="contact.html">Contact</a></li>
                            <li><a href="gallery.html">Gallery</a></li>
                            <li><a href="faq.html">FAQ's</a></li>
                        </ul><!-- /.footer-widget__links-list list-unstyled -->
                    </div><!-- /.footer-widget__column -->
                    <div class="footer-widget__column footer-widget__links">
                        <h3 class="footer-widget__title">Links</h3><!-- /.footer-widget__title -->
                        <ul class="footer-widget__links-list list-unstyled">
                            <li><a href="destinations.html">Destinations</a></li>
                            <li><a href="tour-sidebar.html">Tours</a></li>
                            <li><a href="contact.html">Contact</a></li>
                            <li><a href="gallery.html">Gallery</a></li>
                            <li><a href="faq.html">FAQ's</a></li>
                        </ul><!-- /.footer-widget__links-list list-unstyled -->
                    </div><!-- /.footer-widget__column -->
                   
                    
            </div><!-- /.container -->
        </footer><!-- /.site-footer -->

        <div class="site-footer__bottom">
            <div class="container">
                <p>@ All copyright 2020, <a href="#">cruzia.com</a></p>
                <div class="site-footer__social">
                    <a href="#"><i class="fab fa-facebook-square"></i><!-- /.fab fa-facebook-square --></a>
                    <a href="#"><i class="fab fa-twitter"></i><!-- /.fab fa-twitter --></a>
                    <a href="#"><i class="fab fa-youtube"></i><!-- /.fab fa-instagram --></a>
              
                </div><!-- /.site-footer__social -->
            </div><!-- /.container -->
        </div><!-- /.site-footer__bottom -->

    </div><!-- /.page-wrapper -->


    <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>


    <div class="side-menu__block">


        <div class="side-menu__block-overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.side-menu__block-overlay -->
        <div class="side-menu__block-inner ">
            <div class="side-menu__top justify-content-end">

                <a href="#" class="side-menu__toggler side-menu__close-btn"><img src="assets/images/shapes/close-1-1.png" alt=""></a>
            </div><!-- /.side-menu__top -->


            <nav class="mobile-nav__container">
                <!-- content is loading via js -->
            </nav>
            <div class="side-menu__sep"></div><!-- /.side-menu__sep -->
            <div class="side-menu__content">
                <p>Tripo is a premium wordpress theme for travel, tours, trips, adventures and a wide range of other tour agencies.</p>
                <p><a href="mailto:needhelp@tripo.com">needhelp@tripo.com</a> <br> <a href="tel:888-999-0000">888 999 0000</a></p>
                <div class="side-menu__social">
                    <a href="#"><i class="fab fa-facebook-square"></i></a>
                    <a href="#"><i class="fab fa-twitter"></i></a>
                    <a href="#"><i class="fab fa-youtube"></i></a>
                    
                </div>
            </div><!-- /.side-menu__content -->
        </div><!-- /.side-menu__block-inner -->
    </div><!-- /.side-menu__block -->



    <div class="search-popup">
        <div class="search-popup__overlay custom-cursor__overlay">
            <div class="cursor"></div>
            <div class="cursor-follower"></div>
        </div><!-- /.search-popup__overlay -->
        <div class="search-popup__inner">
            <form action="#" class="search-popup__form">
                <input type="text" name="search" placeholder="Type here to Search....">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
        </div><!-- /.search-popup__inner -->
    </div><!-- /.search-popup -->


    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <script src="assets/js/owl.carousel.min.js"></script>
    <script src="assets/js/waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/TweenMax.min.js"></script>
    <script src="assets/js/wow.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/jquery.ajaxchimp.min.js"></script>
    <script src="assets/js/swiper.min.js"></script>
    <script src="assets/js/typed-2.0.11.js"></script>
    <script src="assets/js/vegas.min.js"></script>
    <script src="assets/js/jquery.validate.min.js"></script>
    <script src="assets/js/bootstrap-select.min.js"></script>
    <script src="assets/js/countdown.min.js"></script>
    <script src="assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="assets/js/bootstrap-datepicker.min.js"></script>
    <script src="assets/js/nouislider.min.js"></script>
    <script src="assets/js/isotope.js"></script>
    <script src="assets/js/rater.min.js"></script>
    <script src="assets/js/custom.js"></script>

    <!-- template scripts -->
    <script src="assets/js/theme.js"></script>
</body>

</html>