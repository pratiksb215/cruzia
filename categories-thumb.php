
<?php require_once ('./includes/header.php') ?>

<section class="page-header" style="background-image: url(assets/images/backgrounds/main-banner.jpg);">
            <div class="container">
                <h2>More Categories</h2>
                <!--
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="index.html">Home</a></li>
                    <li><span>Destination</span></li>
                </ul>--><!-- /.thm-breadcrumb -->
            </div><!-- /.container -->
        </section>


        <section class="tour-one tour-grid">
            <div class="container">
                <div class="tour-sorter-one">
                    <h3>24 Tours Found</h3>
                    <div class="tour-sorter-one__right">
                        <div class="tour-sorter-one__select">
                            <select name="sort-by" id="sort-by" class="selectpicker">
                                <option value="Sort">Sort</option>
                                <option value="By Date">By Date</option>
                                <option value="By Price">By Price</option>
                            </select><!-- /#sort-by .selectpicker -->
                        </div><!-- /.tour-sorter-one__select -->
                        <a href="categories-list.php"><i class="tripo-icon-list-menu"></i></a>
                        <a class="active" href="categories-thumb.php"><i class="tripo-icon-squares"></i></a>
                    </div><!-- /.tour-sorter-one__right -->

                </div><!-- /.tour-sorter-one -->
            
                <div class="row">
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c1.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c2.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c3.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c4.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c5.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c6.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    
                </div><!-- /.row -->





                <div class="post-pagination">
                    <a href="#"><i class="fa fa-angle-left"></i></a>
                    <a class="active" href="#">01</a>
                    <a href="#">02</a>
                    <a href="#">03</a>
                    <a href="#"><i class="fa fa-angle-right"></i></a>
                </div><!-- /.post-pagination -->
            </div><!-- /.container -->
        </section><!-- /.tour-one -->




 <?php require_once ('./includes/footer.php') ?>