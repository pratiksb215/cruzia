<?php require_once('./includes/header.php') ?>

<section class="page-header" style="background-image: url(assets/images/backgrounds/main-banner.jpg);">
    <div class="container">
        <h2>Service Details

        </h2>
        <!--
                <ul class="thm-breadcrumb list-unstyled">
                    <li><a href="index.html">Home</a></li>
                    <li><span>Destination</span></li>
                </ul>-->
        <!-- /.thm-breadcrumb -->
    </div><!-- /.container -->
</section>


<section class="tour-two tour-list service-details">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="tour-details__content">
                    <div class="tour-two__top">
                        <div class="tour-two__top-left">
                            <h3>Relaxing Yacht Services</h3>
                            <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                <div>2 Reviews </div>
                            </div><!-- /.tour-one__stars -->

                        </div><!-- /.tour-two__top-left -->
                        <div class="tour-two__right">
                            <p><span>$1478</span> <br> Per Person</p>
                        </div><!-- /.tour-two__right -->
                    </div><!-- /.tour-two__top -->
                    <div class="address">
                        <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon
                    </div>

                    <div class="tour-details__gallery-carousel">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-image">
                                    <img src="assets/images/tour/tour-d-1-1.jpg" alt="">
                                    <div class="tour-details__gallery-links">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </div><!-- /.tour-details__gallery-links -->
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-image">
                                    <img src="assets/images/tour/tour-d-1-2.jpg" alt="">
                                    <div class="tour-details__gallery-links">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </div><!-- /.tour-details__gallery-links -->
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-image">
                                    <img src="assets/images/tour/tour-d-1-3.jpg" alt="">
                                    <div class="tour-details__gallery-links">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </div><!-- /.tour-details__gallery-links -->
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-image">
                                    <img src="assets/images/tour/tour-d-1-4.jpg" alt="">
                                    <div class="tour-details__gallery-links">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </div><!-- /.tour-details__gallery-links -->
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-image">
                                    <img src="assets/images/tour/tour-d-1-5.jpg" alt="">
                                    <div class="tour-details__gallery-links">
                                        <a href="#"><i class="fab fa-youtube"></i></a>
                                        <a href="#"><i class="fa fa-heart"></i></a>
                                    </div><!-- /.tour-details__gallery-links -->
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                        </div><!-- /.swiper-wrapper -->
                    </div><!-- /.tour-details__gallery-carousel -->

                    <div class="tour-details__gallery-thumb-carousel">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-thumb-image">
                                    <img src="assets/images/tour/tour-thumb-1-1.jpg" alt="">
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-thumb-image">
                                    <img src="assets/images/tour/tour-thumb-1-2.jpg" alt="">
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-thumb-image">
                                    <img src="assets/images/tour/tour-thumb-1-3.jpg" alt="">
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-thumb-image">
                                    <img src="assets/images/tour/tour-thumb-1-4.jpg" alt="">
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                            <div class="swiper-slide">
                                <div class="tour-details__gallery-thumb-image">
                                    <img src="assets/images/tour/tour-thumb-1-5.jpg" alt="">
                                </div><!-- /.tour-details__gallery-image -->
                            </div><!-- /.swiper-slide -->
                        </div><!-- /.swiper-wrapper -->
                    </div><!-- /.tour-details__gallery-thumb-carousel -->
                    <h3 class="tour-details__title">Discription</h3><!-- /.tour-details__title -->
                    <p>Lorem ipsum available isn but the majority have suffered alteratin in some or form injected.
                        Lorem ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum
                        quia quaed inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Lorem
                        ipsum is simply free text used by copytyping refreshing. Neque porro est qui dolorem ipsum quia
                        quaed inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Aelltes port
                        lacus quis enim var sed efficitur turpis gilla sed sit amet finibus eros.</p>


                    <div class="tour-details__spacer"></div><!-- /.tour-details__spacer -->
                    <h3 class="tour-details__title">Including</h3><!-- /.tour-details__subtitle -->
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="tour-details__list list-unstyled">
                                <li>
                                    <i class="fa fa-check"></i>
                                    Pick and Drop Services
                                </li>
                                <li>
                                    <i class="fa fa-check"></i>
                                    1 Meal Per Day
                                </li>
                                <li>
                                    <i class="fa fa-check"></i>
                                    Cruise Dinner & Music Event
                                </li>
                                <li>
                                    <i class="fa fa-check"></i>
                                    Visit 7 Best Places in the City With Group
                                </li>
                            </ul><!-- /.tour-details__list -->
                        </div><!-- /.col-md-6 -->
                        <!--
                                <div class="col-md-6">
                                    <ul class="tour-details__list unavailable list-unstyled">
                                        <li>
                                            <i class="fa fa-times"></i>
                                            Additional Services
                                        </li>
                                        <li>
                                            <i class="fa fa-times"></i>
                                            1 Meal Per Day
                                        </li>
                                        <li>
                                            <i class="fa fa-times"></i>
                                            Insurance
                                        </li>
                                        <li>
                                            <i class="fa fa-times"></i>
                                            Food & Drinks
                                        </li>
                                    </ul>
                                </div> -->
                        <!-- /.col-md-6 -->
                    </div><!-- /.row -->
                    <div class="tour-details__spacer"></div><!-- /.tour-details__spacer -->
                    <div class="owl-carousel review owl-theme">
                        <div class="item">
                            <div class="item_header">
                                <div class="tour-sidebar__organizer-content">
                                    <img src="assets/images/tour/tour-organizer-1-1.jpg" alt="">
                                    <h3>Mike Hardson</h3>
                                    <div class="rate" data-rate-value=4></div>
                                </div>
                                <div class="date">
                                    13/02/2020
                                </div>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                        <div class="item">
                            <div class="item_header">
                                <div class="tour-sidebar__organizer-content">
                                    <img src="assets/images/tour/tour-organizer-1-1.jpg" alt="">
                                    <h3>Mike Hardson</h3>
                                    <div class="rate" data-rate-value=4></div>
                                </div>
                                <div class="date">
                                    13/02/2020
                                </div>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                        <div class="item">
                            <div class="item_header">
                                <div class="tour-sidebar__organizer-content">
                                    <img src="assets/images/tour/tour-organizer-1-1.jpg" alt="">
                                    <h3>Mike Hardson</h3>
                                    <div class="rate" data-rate-value=4></div>
                                </div>
                                <div class="date">
                                    13/02/2020
                                </div>
                            </div>
                            <p>
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>

                    </div>

                    <div class="tour-details__spacer"></div><!-- /.tour-details__spacer -->

                    <h3 class="tour-details__title">Tour Location</h3><!-- /.tour-details__title -->
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4562.753041141002!2d-118.80123790098536!3d34.152323469614075!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80e82469c2162619%3A0xba03efb7998eef6d!2sCostco+Wholesale!5e0!3m2!1sbn!2sbd!4v1562518641290!5m2!1sbn!2sbd" class="google-map__contact google-map__tour-details" allowfullscreen></iframe>




                </div><!-- /.tour-details__content -->

            </div><!-- /.col-lg-8 -->
            <div class="col-lg-4">
                <div class="tour-sidebar">
                    <div class="price-start-from">

                        <abbr>Starts From :</abbr>
                        <span>$200</span>
                        <small>Per Person</small>
                    </div>
                    <div class="tour-sidebar__search tour-sidebar__single">
                        <h3>Check Availability</h3>
                        <form action="#" class="tour-sidebar__search-form">
                           <!-- <div class="input-group">
                                <input type="text" placeholder="Your Name">
                            </div>
                            <div class="input-group">
                                <input type="text" placeholder="Email Address">
                            </div>
                            <div class="input-group">
                                <input type="text" placeholder="Phone">
                            </div>
                            <div class="input-group">
                                <input type="text" data-provide="datepicker" placeholder="dd/mm/yy">
                            </div> -->
                            <div class="input-group">
                                <select class="selectpicker">
                                    <option value="Tickets">No. of Child</option>
                                    <option value="Children">1</option>
                                    <option value="Adult">2</option>
                                </select>
                            </div><!-- /.input-group -->
                            <div class="input-group">
                                <select class="selectpicker">
                                    <option value="Tickets">No. of Adult</option>
                                    <option value="Children">1</option>
                                    <option value="Adult">2</option>
                                </select>
                            </div>
                            <div class="input-group">
                                <input type="text" data-provide="datepicker" placeholder="dd/mm/yy">
                            </div>
                            <!-- 
                            <div class="input-group">
                                <textarea placeholder="Message"></textarea>
                            </div> -->
                            <div class="input-group">
                                <button type="submit" class="thm-btn">Book Now</button>
                            </div><!-- /.input-group -->
                        </form>
                    </div><!-- /.tour-sidebar__search -->
                
                </div><!-- /.tour-sidebar -->
            </div><!-- /.col-lg-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.tour-two -->





<?php require_once('./includes/footer.php') ?>