
<?php require_once ('./includes/header.php') ?>

        <section class="banner-one" style="background-image: url(assets/images/backgrounds/main-banner.jpg);">
            <div class="container">
                <h2>Find your <span>next tour</span></h2>
                <p>Where would you like to go?</p>
                <ul class="banner-heading">
                    <li><a href="#">Today</a></li>
                    <li><a href="#">Tomorrow</a></li>
                    <li><a href="#"><i class="fa fa-calendar-alt"></i></a></li>
                </ul>
            </div><!-- /.container -->
        </section><!-- /.banner-one -->

        <section class="features-one__title">
            <div class="container">
                <div class="block-title text-center">
                    <p>Call us for Booking !</p>
                    <h3>Find Our Most <br> Popular Categories</h3>
                </div><!-- /.block-title -->
            </div><!-- /.container -->
        </section><!-- /.features-one__title -->

        <section class="features-one__content">
            <div class="container">
            <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="destinations-three__single">
                            <img src="assets/images/categories/t1.jpg" alt="">
                            <div class="destinations-three__content">
                                <h3><a href="destinations-details.html">Customised Experiences</a></h3>
                            </div><!-- /.destinations-three__content -->
                            <div class="destinations-three__hover-content">
                                <h3><a href="destinations-details.html">Customised Experiences</a></h3>
                                <!--<p>24 Tours</p>
                                <a href="#" class="destinations-three__link"><i class="tripo-icon-right-arrow"></i></a> -->
                            </div><!-- /.destinations-three__hover-content -->
                        </div><!-- /.destinations-three__single -->
                    </div><!-- /.col-lg-4 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="destinations-three__single">
                            <img src="assets/images/categories/t2.jpg" alt="">
                            <div class="destinations-three__content">
                                <h3><a href="destinations-details.html">Underwater Adventure</a></h3>
                            </div><!-- /.destinations-three__content -->
                            <div class="destinations-three__hover-content">
                                <h3><a href="destinations-details.html">Underwater Adventure</a></h3>
                                 <!--<p>24 Tours</p> 
                                <a href="#" class="destinations-three__link"><i class="tripo-icon-right-arrow"></i></a> -->
                            </div><!-- /.destinations-three__hover-content -->
                        </div><!-- /.destinations-three__single -->
                    </div><!-- /.col-lg-4 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="destinations-three__single">
                            <img src="assets/images/categories/t3.jpg" alt="">
                            <div class="destinations-three__content">
                                <h3><a href="destinations-details.html">Island <br> Tours</a></h3>
                            </div><!-- /.destinations-three__content -->
                            <div class="destinations-three__hover-content">
                                <h3><a href="destinations-details.html">Island <br> Tours</a></h3>
                                <!--<p>24 Tours</p> 
                                <a href="#" class="destinations-three__link"><i class="tripo-icon-right-arrow"></i></a> -->
                            </div><!-- /.destinations-three__hover-content -->
                        </div><!-- /.destinations-three__single -->
                    </div><!-- /.col-lg-4 col-md-6 -->
                    <div class="col-lg-3 col-md-6">
                        <div class="destinations-three__single">
                            <img src="assets/images/categories/t4.jpg" alt="">
                            <div class="destinations-three__content">
                                <h3><a href="destinations-details.html">Adventure Sports</a></h3>
                            </div><!-- /.destinations-three__content -->
                            <div class="destinations-three__hover-content">
                                <h3><a href="destinations-details.html">Adventure Sports</a></h3>
                                <!--<p>24 Tours</p> 
                                <a href="#" class="destinations-three__link"><i class="tripo-icon-right-arrow"></i></a>-->
                            </div><!-- /.destinations-three__hover-content -->
                        </div><!-- /.destinations-three__single -->
                    </div><!-- /.col-lg-4 col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.features-one__content -->

        <section class="tour-one">
            <div class="container">
                <div class="block-title text-center">
                    <p>FEATURED TOURS</p>
                    <h3>Things To Do</h3>
                </div><!-- /.block-title -->
                <div class="row">
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c1.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c2.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c3.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c4.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c5.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-xl-4 col-lg-6">
                        <div class="tour-one__single">
                            <div class="tour-one__image">
                                <img src="assets/images/categories/c6.jpg" alt="">
                                <a href="tour-details.html"><i class="fa fa-heart"></i></a>
                            </div><!-- /.tour-one__image -->
                            <div class="tour-one__content">
                                <h3><a href="tour-details.html">National Park 2 Days Tour</a></h3>
                                <div class="tour-one__stars">
                                <div class="rate" data-rate-value=4></div>
                                </div><!-- /.tour-one__stars -->
                                <div class="address">
                                    <i class="fa fa-map-marker-alt"></i> Canary Islands, Tenerife, Puerto Colon 
                                </div>
                              
                                <ul class="tour-one__meta list-unstyled">
                                    <li><p>Start From <span>$1870</span></p></li>
                                  
                                </ul><!-- /.tour-one__meta -->
                            </div><!-- /.tour-one__content -->
                        </div><!-- /.tour-one__single -->
                    </div><!-- /.col-lg-4 -->
                    
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.tour-one -->

        <section class="video-one mid-banner" style="background-image: url(assets/images/backgrounds/mid-banner.jpg);">
            <div class="container text-center">
                <p>Lorem Ipsum</p>
                <h3>Love where <abbr>you’re going</abbr></h3>
                <a href="#">Call Now</a>
            </div><!-- /.container -->
        </section><!-- /.video-one -->


      
        <section class="testimonials-one">
            <div class="container">
                <div class="block-title text-center">
                    <p>checkout our</p>
                    <h3>Testimonials</h3>
                </div><!-- /.block-title -->
                <div class="testimonials-one__carousel thm__owl-carousel light-dots owl-carousel owl-theme" data-options='{"nav": false, "autoplay": true, "autoplayTimeout": 5000, "smartSpeed": 700, "dots": true, "margin": 30, "loop": true, "responsive": { "0": { "items": 1, "nav": true, "navText": ["Prev", "Next"], "dots": false }, "767": { "items": 1, "nav": true, "navText": ["Prev", "Next"], "dots": false }, "991": { "items": 2 }, "1199": { "items": 2 }, "1200": { "items": 3 } }}'>
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-1.jpg" alt="">
                                <h3>Kevin Smith</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-2.jpg" alt="">
                                <h3>Christine Eve</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-3.jpg" alt="">
                                <h3>Mike Hardson</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-1.jpg" alt="">
                                <h3>Kevin Smith</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-2.jpg" alt="">
                                <h3>Christine Eve</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-3.jpg" alt="">
                                <h3>Mike Hardson</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-1.jpg" alt="">
                                <h3>Kevin Smith</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-2.jpg" alt="">
                                <h3>Christine Eve</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                    <div class="item">
                        <div class="testimonials-one__single">
                            <div class="testimonials-one__content">
                                <div class="testimonials-one__stars">
                                <div class="rate" data-rate-value=5></div>
                                </div><!-- /.testimonials-one__stars -->
                                <p>There are many variations of passages of lorem ipsum but the majority have alteration in some
                                    form, by randomised words look. Aene an commodo ligula eget dolorm sociis.</p>
                            </div><!-- /.testimonials-one__content -->
                            <div class="testimonials-one__info">
                                <img src="assets/images/testimonials/testimonials-1-3.jpg" alt="">
                                <h3>Mike Hardson</h3>
                            </div><!-- /.testimonials-one__info -->
                        </div><!-- /.testimonials-one__single -->
                    </div><!-- /.item -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.testimonials-one -->

      


 <?php require_once ('./includes/footer.php') ?>